# How to run

```
docker run --rm -v ./src:/opt/te registry.gitlab.com/mazoea-team/docker-validate-json-schema
```

# How to update

Simply commit to the repository.